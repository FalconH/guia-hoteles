$(function () {
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
            interval:2000
        });

        $('#reserva').on('show.bs.modal', function (e){
          console.log('el modal contacto se esta mostrando');

          $('#reservaBtn').removeClass('btn-primary');
          $('#reservaBtn').addClass('btn-secondary');
          $('#reservaBtn').prop('disabled', true);

        });
        $('#reserva').on('shown.bs.modal', function (e){
          console.log('el modal reserva se mostró');
        });
        $('#reserva').on('hide.bs.modal', function (e){
          console.log('el modal reserva se esta ocultando');
        });
        $('#reserva').on('hidden.bs.modal', function (e){
          console.log('el modal reserva se ocultó');
          $('#reservaBtn').prop('disabled', false);
        });
      });